# number system converter
Simple and lightweight number system converter written in C++ in Qt.

Work with binary, octal, decimal, hexadecimal systems.

![Imgur](http://i.imgur.com/vNxAWjN.png)
