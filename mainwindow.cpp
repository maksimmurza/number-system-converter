#include <QMessageBox>
#include <QString>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <math.h>
#include <string.h>
#include <string>

using namespace std;

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

/*
    All kinds of convertings are realized with help of binary number system.
    At first, we translate numeral from 8, 10, 16 number systems to binary,
    and then from binary to selected number system.

    This case allow to avoid writing code for every variant of converting.
    We writeв only 2 methods:
    - from binary to all
    - from all to binary
*/

int check_input(QString incorrect_symbols, QString hexadecimal_symbols, QString check, int sys_in, int sys_out);
QString FromBinary(QString string_input, int sys_out);
QString ToBinary(QString string_input, int sys_in);

void MainWindow::on_enter_clicked()
{
    int sys_in, sys_out;
    QString output_string,
            input_string,
            incorrect_symbols = "!@#$%^&*()-+=_?|><,.;:'qwe\rtyuiop[]as/dfghjklzxcvbnmQWRTYUIOPSGHJKLZXVNM",
            hexadecimal_symbols = "ABCDEF";

    if(ui->Input2->isChecked())
        sys_in = 2;
    else if(ui->Input8->isChecked())
        sys_in = 8;
    else if(ui->Input10->isChecked())
        sys_in = 10;
    else if(ui->Input16->isChecked())
        sys_in = 16;

    if(ui->Output2->isChecked())
        sys_out = 2;
    else if(ui->Output8->isChecked())
        sys_out = 8;
    else if(ui->Output10->isChecked())
        sys_out = 10;
    else if(ui->Output16->isChecked())
        sys_out = 16;

    input_string = ui->lineEdit->text();

    if(check_input(incorrect_symbols, hexadecimal_symbols, input_string, sys_in, sys_out))
    {
        int button = QMessageBox::warning(this, "Warning", "Incorrect input!", QMessageBox::Ok);
    }
    else
    {
        if(sys_in == sys_out)
            ui->lineEdit_2->setText(input_string);
        else
        {
            if(sys_in != 2)
            {
                if(sys_out!=2)
                    output_string = FromBinary(ToBinary(input_string, sys_in), sys_out);
                else
                    output_string = ToBinary(input_string, sys_in);
            }
            else
               output_string = FromBinary(input_string, sys_out);

            ui->lineEdit_2->setText(output_string);
        }
    }
}

int check_input(QString incorrect_symbols, QString hexadecimal_symbols, QString input_string, int sys_in, int sys_out)
{
    // Search of invalid characters
    for(int i = 0; i < input_string.length(); i++)
    {
        for(int k = 0; k < incorrect_symbols.length(); k++)
        {
            if(input_string[i] == incorrect_symbols[k])
            {
                return 1;
            }
        }
    }

    // Search of numerals and characters, which are don't match to the selected number system
    switch(sys_in)
    {
        case 2:
            for(int i = 0; i < input_string.length(); i++)
            {
                if((input_string.toStdString()[i]- '0' != 0) && (input_string.toStdString()[i]- '0' != 1))
                {
                    return 1;
                }
            }
            break;

        case 8:
            for(int i = 0; i < input_string.length(); i++)
            {
                if(input_string.toStdString()[i]- '0' == 9)
                {
                    return 1;
                }

                for(int i = 0; i < input_string.length(); i++)
                {
                    for(int k = 0; k < hexadecimal_symbols.length(); k++)
                    {
                        if(input_string[i] == hexadecimal_symbols[k])
                        {
                            return 1;
                        }
                    }
                }
            }
            break;

        case 10:
            for(int i = 0; i < input_string.length(); i++)
            {
                for(int k = 0; k < hexadecimal_symbols.length(); k++)
                {
                    if(input_string[i] == hexadecimal_symbols[k])
                    {
                        return 1;
                    }
                }
            }
            break;

        case 16:
            break;
    }
    return 0;
}

/*
    We use normal process of converting if number system of output = 10:
    multiplying every discharge (0 or 1) on the '2' in degree which is match to
    discharge's place (begining with 0).

    If number system of output is 8 or 16, we divide numeral on the parts.
    Count of the numerals in the every part is a degree of '2', which is
    constitutes selected number system (2^3=8 or 2^4=16). Then every
    part we translate into '10'. And replace received result with letters
    if it is necessary. In this way we form string which is a result.
*/

QString FromBinary(QString string_input, int sys_out)
{
    int x, m, r, k, n,
        discharge,
        output = 0,
        cnt_null,
        part_output=0;

    double i=0, j;

    QString string_output,
            buf,
            word;

    if(sys_out == 10)
    {
        for(i=0, n=string_input.length()-1; i<string_input.length(); i++, n--)
        {
            discharge = string_input.toStdString()[n] - '0';
            output += discharge * pow(2, i);
        }
        string_output = QString::number(output);
    }
    else
    {
        // r is a degree of '2', which is constitutes selected number system (2^3=8 or 2^4=16)
        switch(sys_out)
        {
            case 8: r = 3; break;
            case 16: r = 4; break;
        }

        // fill with zeros to the full set (full set = 4 for sys_out = 16 and 3 for 8)
        if(string_input.length()%r > 0)
        {
            cnt_null = r - (string_input.length()%r);

            for(i=0; i<cnt_null; i++)
            {
                string_input.prepend("0");
            }
        }

        // translate every part into '10'
        for(i=string_input.length()-r, m=0; i>=0; i-=r, m++)
        {
            // form part for translating
            part_output = 0;
            for(k=i,x=0; x<r; k++,x++)
            {
                buf[x] = string_input[k];
            }

            // translate formed part into '10'
            for(j=0, n=buf.length()-1; j<buf.length(); j++, n--)
            {
                discharge = buf.toStdString()[n] - '0';
                part_output += discharge * pow(2, j);
            }

            // if sys_out = 16, use capital letters instead of numerals > 9
            if((sys_out == 16) && (part_output > 9))
            {
                switch(part_output)
                {
                    case 10:
                        word = "A";
                        break;
                    case 11:
                        word = "B";
                        break;
                    case 12:
                        word = "C";
                        break;
                    case 13:
                        word = "D";
                        break;
                    case 14:
                        word = "E";
                        break;
                    case 15:
                        word = "F";
                        break;
                }

                // Ascribe to the result word
                string_output.prepend(word);

            }
            else
                // Ascribe to the result translated part
                string_output.prepend(QString::number(part_output));
        }
    }
    return string_output;
}

/*
    Convering 10 -> 2 is realized with help of successive dividing on '2'.
    Result is consist of balances of this dividing, which are written one
    after the other in the reverse order.

    Convertings 8 -> 2 and 16 -> 2 are based on the 2 -> 10 converting.
    Every character of numerals in 8 or 16 n.s. is translating into '10'
    and results of this convertings are builded main result.
*/

QString ToBinary(QString string_input, int sys_in)
{
    int j=0, k, n,
        input = string_input.toInt(),
        discharge,
        output = 0,
        cnt_null,
        balance,
        temp;

    double i=0;

    QString string_output,
            buf_input,
            buf_output,
            invert;

    switch(sys_in)
    {
        case 8:
            for(j=string_input.length()-1; j>=0; j--)
            {
                buf_input = string_input[j];
                buf_output = ToBinary(buf_input, 10);
                if((buf_output.length()<3) && (j!=0))
                {
                    cnt_null = 3 - buf_output.length();
                    for(i = 0; i < cnt_null; i++)
                    {
                       buf_output.prepend("0");
                    }
                }
                string_output.prepend(buf_output);
            }

            return string_output;
            break;

        case 10:
            while(input >= 2)
            {
                balance = input%2;
                invert[j] = balance + '0';
                input = input/2;
                j++;
            }

            invert[invert.length()] = input + '0';
            for(j=0, n=invert.length()-1; j<invert.length(); j++, n--)
            {
                string_output[n] = invert[j];
            }
            return string_output;
            break;

        case 16:
            for(j=string_input.length()-1; j>=0; j--)
            {
                buf_input = string_input[j];

                if(buf_input == "A")
                    buf_input = "10";
                else if(buf_input == "B")
                        buf_input = "11";
                else if(buf_input == "C")
                        buf_input = "12";
                else if(buf_input == "D")
                        buf_input = "13";
                else if(buf_input == "E")
                        buf_input = "14";
                else if(buf_input == "F")
                        buf_input = "15";

                buf_output = ToBinary(buf_input, 10);
                if((buf_output.length()<4) && (j!=0))
                {
                    cnt_null = 4 - buf_output.length();
                    for(i = 0; i < cnt_null; i++)
                    {
                       buf_output.prepend("0");
                    }
                }

                string_output.prepend(buf_output);
            }

            return string_output;
            break;
    }
}

void MainWindow::on_clean_clicked()
{
    ui->lineEdit->clear();
    ui->lineEdit_2->clear();
}

void MainWindow::on_exit_clicked()
{
    int button = QMessageBox::question(this, "Exit", "Are you really want to leave program?", QMessageBox::Ok|QMessageBox::Cancel);
    if(button == QMessageBox::Ok)
        exit(0);
}

void MainWindow::on_replace_clicked()
{
    QString buf;
    buf = ui->lineEdit->text();
    ui->lineEdit->setText(ui->lineEdit_2->text());
    ui->lineEdit_2->setText(buf);
}

void MainWindow::on_info_clicked()
{
    int button = QMessageBox::information(this, "Info", "Writted by Maksim Murza\n(github.com/charadziej)\n"
                                                        "Belarus, Brest, 2016", QMessageBox::Ok);
}
